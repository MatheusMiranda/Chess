package testes;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import modelo.Bispo;
import modelo.Cavalo;
import modelo.Peao;
import modelo.Peca;
import modelo.Rainha;
import modelo.Rei;
import modelo.Square;
import modelo.Torre;

import org.junit.Assert;
import org.junit.Test;

import control.SquareControl;

public class PecaTest {

	@Test
	public void testMovePeao() throws Exception {
		SquareControl squarecontrol = new SquareControl();
		Square square = squarecontrol.getSquare(4,4);
		Peca p = new Peao("Peao","Piecepath",Color.BLACK);
		Peca p2 = new Peao("Peao","Piecepath",Color.WHITE);
		square.setPeca(p);
		assertMoves(MovesTestHelper.getPawnMoves(Color.BLACK),p.move(squarecontrol,square));
		square.setPeca(p2);
		assertMoves(MovesTestHelper.getPawnMoves(Color.WHITE),p2.move(squarecontrol,square));
	}

	@Test
	public void testMoveTorre() throws Exception {
		SquareControl squarecontrol = new SquareControl();
		Peca p = new Torre("Torre","Piecepath",Color.BLACK);
		Square square = squarecontrol.getSquare(4,4);

		assertMoves(MovesTestHelper.getTowerMoves(), p.move(squarecontrol,square));
	}

	@Test
	public void testMoveCavalo() throws Exception{
		SquareControl squarecontrol = new SquareControl();
		Peca p = new Cavalo("Cavalo","Piecepath",Color.BLACK);
		Square square = squarecontrol.getSquare(4,4);

		assertMoves(MovesTestHelper.getHorseMoves(), p.move(squarecontrol,square));	
	}

	@Test
	public void testMoveBispo() throws Exception{
		SquareControl squarecontrol = new SquareControl();
		Peca p = new Bispo("Bispo","Piecepath",Color.BLACK);
		Square square = squarecontrol.getSquare(4,4);

		assertMoves(MovesTestHelper.getBishopMoves(), p.move(squarecontrol,square));
	}

	@Test
	public void testMoveRei() throws Exception{
		SquareControl squarecontrol = new SquareControl();
		Peca p = new Rei("Rei","Piecepath",Color.BLACK);
		Square square = squarecontrol.getSquare(4,4);

		assertMoves(MovesTestHelper.getKingMoves(), p.move(squarecontrol,square));
	}

	@Test
	public void testMoveRainha() throws Exception{
		SquareControl squarecontrol = new SquareControl();
		Peca p = new Rainha("Rainha","Piecepath",Color.BLACK);
		Square square = squarecontrol.getSquare(4,4);

		assertMoves(MovesTestHelper.getQueenMoves(), p.move(squarecontrol,square));
	}

	private void assertMoves(ArrayList<Point> movesA, ArrayList<Point> movesB) throws Exception {
		if (movesA.size() != movesB.size()) {
			Assert.assertTrue(false);
			return;
		}

		for (Point point : movesA) {
			if (!movesB.contains(point)) {
				Assert.assertTrue(false);
				return;
			}
		}
		Assert.assertTrue(true);
	}

}
