package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import modelo.Bispo;
import modelo.Cavalo;
import modelo.Peao;
import modelo.Peca;
import modelo.Rainha;
import modelo.Rei;
import modelo.Square;
import modelo.Torre;
import control.SquareControl;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}


	private void initializePiecesInChess() {
		Peca p = new Peao("Peao","icon/Brown P_48x48.png",Color.BLACK);
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(1, i).setPeca(p);
		}

		Peca t = new Torre("Torre","icon/Brown R_48x48.png",Color.BLACK);
		this.squareControl.getSquare(0, 0).setPeca(t);
		this.squareControl.getSquare(0, 7).setPeca(t);

		Peca c = new Cavalo("Cavalo","icon/Brown N_48x48.png",Color.BLACK);
		this.squareControl.getSquare(0, 1).setPeca(c);
		this.squareControl.getSquare(0, 6).setPeca(c);

		Peca b = new Bispo("Bispo","icon/Brown B_48x48.png",Color.BLACK);
		this.squareControl.getSquare(0, 2).setPeca(b);
		this.squareControl.getSquare(0, 5).setPeca(b);

		Peca rainha = new Rainha("Rainha","icon/Brown Q_48x48.png",Color.BLACK);
		this.squareControl.getSquare(0, 4).setPeca(rainha);

		Peca rei = new Rei("Rei","icon/Brown K_48x48.png",Color.BLACK);
		this.squareControl.getSquare(0, 3).setPeca(rei);

		Peca p2 = new Peao("Peao","icon/White P_48x48.png",Color.WHITE);
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(6, i).setPeca(p2);
		}

		Peca t2 = new Torre("Torre","icon/White R_48x48.png",Color.WHITE);
		this.squareControl.getSquare(7, 0).setPeca(t2);
		this.squareControl.getSquare(7, 7).setPeca(t2);

		Peca c2 = new Cavalo("Cavalo","icon/White N_48x48.png",Color.WHITE);
		this.squareControl.getSquare(7, 1).setPeca(c2);
		this.squareControl.getSquare(7, 6).setPeca(c2);

		Peca b2= new Bispo("Bispo","icon/White B_48x48.png",Color.WHITE);
		this.squareControl.getSquare(7, 2).setPeca(b2);
		this.squareControl.getSquare(7, 5).setPeca(b2);

		Peca rainha2 = new Rainha("Rainha","icon/White Q_48x48.png",Color.WHITE);
		this.squareControl.getSquare(7, 4).setPeca(rainha2);

		Peca rei2 = new Rei("Rei","icon/White K_48x48.png",Color.WHITE);
		this.squareControl.getSquare(7, 3).setPeca(rei2);

	}
}
