package control;

import javax.swing.JOptionPane;

public class ExcecaoMovimento extends Throwable{

	private static final long serialVersionUID = 1L;

	public ExcecaoMovimento(String message) {
		super(message);
	}
	public void mensagem(String mensagem){

		JOptionPane.showMessageDialog(null,mensagem,"Movimento inválido",JOptionPane.INFORMATION_MESSAGE);

	}

}
