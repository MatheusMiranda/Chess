package control;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import modelo.Peca;
import modelo.Square;
import modelo.Square.SquareEventListener;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;
	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;
	private Square selectedSquare;
	private ArrayList<Square> squareList;
	private int flag;
	private int flag2 = 0;
	private int jogada;
	private int l = 0;
	private int d = 0;


	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,Color colorSelected) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;
		this.squareList = new ArrayList<>();
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
		if(square.getColor()==Color.GREEN){
			estavaVerde(1);
			square.setColor(Color.CYAN);
		}
		else{ 
			estavaVerde(2);
			square.setColor(this.colorHover);
			if(flag2==0){
				turnos();
				flag2++;
			}
		}

	}

	public void estavaVerde(int valor){
		if(valor == 1){
			flag = 1;		
		}
		else{
			flag = 2;
		}		
	}

	@Override
	public void onSelectEvent(Square square) {
		if (haveSelectedCellPanel()) {	

			try{
				if (!this.selectedSquare.equals(square) && square.getColor()==Color.CYAN){
					moveContentOfSelectedSquare(square);
					this.jogada++;
					flag = 2;
					limpaTabuleiro(squareList);
				} else if(this.selectedSquare.equals(square)){
					unselectSquare(square);
					limpaTabuleiro(squareList);
				}else{
					unselectSquare(square);
					limpaTabuleiro(squareList);
					throw new ExcecaoMovimento("Movimento inválido");
				}
			}
			catch(ExcecaoMovimento ex){
				ex.mensagem("Por favor jogue novamente");
			}

		} else {
			selectSquare(square);
		}
	}

	private boolean testaVez(int jogada){

		if(jogada%2==0){
			return true;
		}
		else{
			return false;
		}

	}

	@Override
	public void onOutEvent(Square square) {

		if (this.selectedSquare != square && flag!=1) {
			resetColor(square);
		} else {
			square.setColor(this.colorSelected);

		}
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) {
		Peca p = square.getPeca();
		if(square.getPeca()!=null){
			if(p.getNome()=="Rei"){
				if(square.getPeca().getCor()==Color.BLACK){
					mensagemFimDeJogo("O jogador branco venceu!!!");
				}
				else{
					mensagemFimDeJogo("O jogador marrom venceu!!!");
				}
			}
		}
		square.setPeca(this.selectedSquare.getPeca());
		this.selectedSquare.removePeca();
		unselectSquare(square);
		resetColor(square);	
		logicaDeXeque();

	}

	private void selectSquare(Square square) {
		if (square.havePeca()) {

			Peca peca= square.getPeca();
			if(square.getPeca().getCor()==Color.WHITE && testaVez(jogada)){
				this.selectedSquare = square;
				peca.move(this,square);
			}
			else if(square.getPeca().getCor()==Color.BLACK && (testaVez(jogada) == false)){
				this.selectedSquare = square;
				peca.move(this,square);		
			}
			else{
				turnos();
			}

		}						
	}

	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
		this.selectedSquare = EMPTY_SQUARE;
	}

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}

	private void limpaTabuleiro(ArrayList<Square> squareList){
		Square square;
		for (int i = 0; i <64; i++){
			square = squareList.get(i);
			resetColor(square);
		}
	}

	public Square getSelectedSquare(){
		return selectedSquare;
	}

	private void mensagem(String mensagem){

		if(l==0){
			JOptionPane.showMessageDialog(null,mensagem,"Bem-vindo,vamos jogar!!",JOptionPane.INFORMATION_MESSAGE);
			l++;
		}	
		else{
			JOptionPane.showMessageDialog(null,mensagem,"Aguarde...",JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private void mensagemFimDeJogo(String mensagem){
		JOptionPane.showMessageDialog(null,mensagem,"O Rei foi capturado",JOptionPane.INFORMATION_MESSAGE);
	}

	private void turnos(){
		if(jogada==0){
			mensagem("O jogador Branco começa");
		}
		else{
			if(testaVez(jogada)){
				mensagem("É a vez do jogador Branco");
			}
			else{
				mensagem("É a vez do jogador Marrom");
			}
		}

	}

	private void logicaDeXeque(){

		Square square2;

		for (int i = 0; i <64; i++){
			square2 = this.squareList.get(i);

			if(square2.getPeca()!=null){

				Peca peca = square2.getPeca();	

				if(square2.getPeca().getCor()==Color.BLACK){
					peca.tstXeque(this,Color.BLACK,square2);
					if(d==1){
						d = 0;
						break;
					}
				}
				else{
					peca.tstXeque(this,Color.WHITE,square2);
					if(d==1){
						d = 0;
						break;
					}
				}
			}	
		}
	}

	public void valida(int i){
		if(i==1){
			d++;
		}
	}

	public void mensagem2(String mensagem){

		JOptionPane.showMessageDialog(null,mensagem,"Atenção",JOptionPane.INFORMATION_MESSAGE);

	}


}



