package modelo;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import control.SquareControl;

public class Rainha extends Peca{
	public Rainha(String nome,String piecePath,Color cor){
		super(nome,piecePath,cor);
	}

	@Override
	public ArrayList<Point> move(SquareControl squarecontrol,Square square){

		Point point = square.getPosition();
		square.setColor(Color.GREEN);	
		ArrayList<Point> moves = new ArrayList<Point>();
		int x=point.x;
		int y=point.y;

		for(int i = point.y+1; i < SquareControl.COL_NUMBER; i++){

			Point point2 = new Point(x,i);
			moves.add(point2);

			if(squarecontrol.getSquare(point.x,i).getPeca()==null){
				squarecontrol.getSquare(point.x,i).setColor(Color.GREEN);
			}
			if(squarecontrol.getSquare(point.x,i).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x,i).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x,i).setColor(Color.GREEN);
					break;
				}
				else{
					break;
				}
			}

		}

		for(int i = point.y-1;i>=0;i--){

			Point point2 = new Point(x,i);
			moves.add(point2);

			if(squarecontrol.getSquare(point.x,i).getPeca()==null ){
				squarecontrol.getSquare(point.x,i).setColor(Color.GREEN);
			}
			if(squarecontrol.getSquare(point.x,i).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x,i).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x,i).setColor(Color.GREEN);
					break;
				}
				else{
					break;
				}
			}
		}
		for (int i = point.x+1; i < SquareControl.COL_NUMBER; i++){

			Point point2 = new Point(i,y);
			moves.add(point2);

			if(squarecontrol.getSquare(i,point.y).getPeca()==null){
				squarecontrol.getSquare(i,point.y).setColor(Color.GREEN);
			}
			if(squarecontrol.getSquare(i,point.y).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(i,point.y).getPeca().getCor()!=cor){
					squarecontrol.getSquare(i,point.y).setColor(Color.GREEN);
					break;
				}
				else{
					break;
				}
			}
		}
		for (int i = point.x-1;i>=0; i--){

			Point point2 = new Point(i,y);
			moves.add(point2);

			if(squarecontrol.getSquare(i,point.y).getPeca()==null){
				squarecontrol.getSquare(i,point.y).setColor(Color.GREEN);
			}
			if(squarecontrol.getSquare(i,point.y).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(i,point.y).getPeca().getCor()!=cor){
					squarecontrol.getSquare(i,point.y).setColor(Color.GREEN);
					break;
				}
				else{
					break;
				}
			}
		}	

		for(int i = point.x+1, j = point.y+1;( i <=7 && j <=7) ; i++, j++) {	

			Point point2 = new Point(i,j);
			moves.add(point2);

			if(squarecontrol.getSquare(i,j).getPeca()==null){
				squarecontrol.getSquare(i,j).setColor(Color.GREEN);
			}
			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					squarecontrol.getSquare(i,j).setColor(Color.GREEN);
					break;
				}
				else{
					break;
				}
			}

		}

		for(int i = point.x-1, j = point.y+1; (i >=0 && j <=7) ; i--, j++) {	

			Point point2 = new Point(i,j);
			moves.add(point2);

			if(squarecontrol.getSquare(i,j).getPeca()==null){
				squarecontrol.getSquare(i,j).setColor(Color.GREEN);
			}
			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					squarecontrol.getSquare(i,j).setColor(Color.GREEN);
					break;
				}
				else{
					break;
				}
			}

		}

		for(int i = point.x-1, j = point.y-1;( i>=0 && j>=0) ; i--, j--) {	

			Point point2 = new Point(i,j);
			moves.add(point2);

			if(squarecontrol.getSquare(i,j).getPeca()==null){
				squarecontrol.getSquare(i,j).setColor(Color.GREEN);
			}
			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					squarecontrol.getSquare(i,j).setColor(Color.GREEN);
					break;
				}
				else{
					break;
				}
			}

		}

		for(int i = point.x+1, j = point.y-1;( i<=7 && j>=0) ; i++, j--) {

			Point point2 = new Point(i,j);
			moves.add(point2);

			if(squarecontrol.getSquare(i,j).getPeca()==null){
				squarecontrol.getSquare(i,j).setColor(Color.GREEN);
			}
			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					squarecontrol.getSquare(i,j).setColor(Color.GREEN);
					break;
				}
				else{
					break;
				}
			}

		}
		return moves;	
	}

	@Override
	public void tstXeque(SquareControl squarecontrol,Color cor,Square square){
		Point point = square.getPosition();

		for(int i = point.y+1; i < SquareControl.COL_NUMBER; i++) {


			if(squarecontrol.getSquare(point.x,i).getPeca()!=null){
				if(squarecontrol.getSquare(point.x,i).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(point.x,i).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(point.x,i).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
							break;
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
							break;
						}
					}
					else{
						break;
					}

				}
				else{
					break;
				}
			}
		}


		for(int i = point.y-1;i>=0;i--){

			if(squarecontrol.getSquare(point.x,i).getPeca()!=null){
				if(squarecontrol.getSquare(point.x,i).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(point.x,i).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(point.x,i).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
							break;
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
							break;
						}
					}
					else{
						break;
					}

				}
				else{
					break;
				}
			}
		}

		for (int i = point.x+1; i < SquareControl.COL_NUMBER; i++){

			if(squarecontrol.getSquare(i,point.y).getPeca()!=null){
				if(squarecontrol.getSquare(i,point.y).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(i,point.y).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(i,point.y).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
							break;
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
							break;
						}
					}
					else{
						break;
					}			
				}
				else{
					break;
				}
			}

		}
		for (int i = point.x-1;i>=0; i--){


			if(squarecontrol.getSquare(i,point.y).getPeca()!=null){
				if(squarecontrol.getSquare(i,point.y).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(i,point.y).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(i,point.y).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
							break;
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
							break;
						}
					}
					else{
						break;
					}			
				}
				else{
					break;
				}
			}

		}


		for(int i = point.x+1, j = point.y+1;( i <=7 && j <=7) ; i++, j++) {					
			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(i,j).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(i,j).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
							break;
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
							break;
						}
					}
					else{
						break;
					}
				}
				else{
					break;
				}
			}

		}

		for(int i = point.x-1, j = point.y+1; (i >=0 && j <=7) ; i--, j++) {					


			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(i,j).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(i,j).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
							break;
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
							break;
						}
					}
					else{
						break;
					}
				}
				else{
					break;
				}
			}

		}

		for(int i = point.x-1, j = point.y-1;( i>=0 && j>=0) ; i--, j--) {					


			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(i,j).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(i,j).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
							break;
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
							break;
						}
					}
					else{
						break;
					}
				}
				else{
					break;
				}
			}
		}	


		for(int i = point.x+1, j = point.y-1;( i<=7 && j>=0) ; i++, j--) {					

			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(i,j).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(i,j).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
							break;
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
							break;
						}
					}
					else{
						break;
					}
				}
				else{
					break;
				}
			}

		}

	}		

}
