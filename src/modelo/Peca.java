package modelo;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import control.SquareControl;

public abstract class Peca {

	private String nome;
	private String piecePath;
	private Color cor;

	public Peca(String nome,String piecePath,Color cor) {
		this.piecePath = piecePath;
		this.nome = nome;
		this.cor = cor;
	}

	public String getPiecePath() {
		return piecePath;
	}

	public void setPiecePath(String piecePath) {
		this.piecePath = piecePath;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public Color getCor() {
		return cor;
	}

	public void setCor(Color cor) {
		this.cor = cor;
	}

	public abstract ArrayList<Point> move(SquareControl squarecontrol,Square square);

	public abstract void tstXeque(SquareControl squarecontrol,Color cor,Square square);

}


