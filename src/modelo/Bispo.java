package modelo;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import control.SquareControl;

public class Bispo extends Peca{

	public Bispo(String nome,String piecePath,Color cor){
		super(nome,piecePath,cor);

	}

	@Override
	public ArrayList<Point> move (SquareControl squarecontrol,Square square){

		Point point = square.getPosition();
		square.setColor(Color.GREEN);	
		ArrayList<Point> moves = new ArrayList<Point>();

		for(int i = point.x+1, j = point.y+1;( i <=7 && j <=7) ; i++, j++) {	

			Point point2 = new Point(i,j);
			moves.add(point2);

			if(squarecontrol.getSquare(i,j).getPeca()==null){
				squarecontrol.getSquare(i,j).setColor(Color.GREEN);
			}
			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					squarecontrol.getSquare(i,j).setColor(Color.GREEN);
					break;
				}
				else{
					break;
				}
			}

		}

		for(int i = point.x-1, j = point.y+1; (i >=0 && j <=7) ; i--, j++) {	

			Point point2 = new Point(i,j);
			moves.add(point2);

			if(squarecontrol.getSquare(i,j).getPeca()==null){
				squarecontrol.getSquare(i,j).setColor(Color.GREEN);
			}
			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					squarecontrol.getSquare(i,j).setColor(Color.GREEN);
					break;
				}
				else{
					break;
				}
			}

		}

		for(int i = point.x-1, j = point.y-1;( i>=0 && j>=0) ; i--, j--) {	

			Point point2 = new Point(i,j);
			moves.add(point2);

			if(squarecontrol.getSquare(i,j).getPeca()==null){
				squarecontrol.getSquare(i,j).setColor(Color.GREEN);
			}
			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					squarecontrol.getSquare(i,j).setColor(Color.GREEN);
					break;
				}
				else{
					break;
				}
			}

		}

		for(int i = point.x+1, j = point.y-1;( i<=7 && j>=0) ; i++, j--) {

			Point point2 = new Point(i,j);
			moves.add(point2);

			if(squarecontrol.getSquare(i,j).getPeca()==null){
				squarecontrol.getSquare(i,j).setColor(Color.GREEN);
			}
			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					squarecontrol.getSquare(i,j).setColor(Color.GREEN);
					break;
				}
				else{
					break;
				}
			}

		}

		return moves;

	}

	@Override
	public void tstXeque(SquareControl squarecontrol,Color cor,Square square){
		Point point = square.getPosition();
		for(int i = point.x+1, j = point.y+1;( i <=7 && j <=7) ; i++, j++) {					
			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(i,j).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(i,j).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
							break;
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
							break;
						}
					}
					else{
						break;
					}
				}
				else{
					break;
				}
			}

		}

		for(int i = point.x-1, j = point.y+1; (i >=0 && j <=7) ; i--, j++) {					


			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(i,j).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(i,j).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
							break;
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
							break;
						}
					}
					else{
						break;
					}
				}
				else{
					break;
				}
			}

		}

		for(int i = point.x-1, j = point.y-1;( i>=0 && j>=0) ; i--, j--) {					


			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(i,j).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(i,j).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
							break;
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
							break;
						}
					}
					else{
						break;
					}
				}
				else{
					break;
				}
			}
		}	


		for(int i = point.x+1, j = point.y-1;( i<=7 && j>=0) ; i++, j--) {					

			if(squarecontrol.getSquare(i,j).getPeca()!=null){
				if(squarecontrol.getSquare(i,j).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(i,j).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(i,j).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
							break;
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
							break;
						}
					}
					else{
						break;
					}
				}
				else{
					break;
				}
			}

		}

	}

}
