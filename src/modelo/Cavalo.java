package modelo;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import control.SquareControl;

public class Cavalo extends Peca {

	public Cavalo(String nome,String piecePath,Color cor){
		super(nome,piecePath,cor);
	}

	@Override
	public ArrayList<Point> move(SquareControl squarecontrol,Square square){

		Point point = square.getPosition();
		square.setColor(Color.GREEN);	
		ArrayList<Point> moves = new ArrayList<Point>();

		if(point.y<6 && point.x<7){
			Point point2 = new Point(point.x+1,point.y+2);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x+1,point.y+2).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x+1,point.y+2).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x+1,point.y+2).setColor(Color.GREEN);
				}
			}	
			else if(squarecontrol.getSquare(point.x+1,point.y+2).getPeca()==null){
				squarecontrol.getSquare(point.x+1,point.y+2).setColor(Color.GREEN);
			}
		}

		if(point.y>1 && point.x<7){
			Point point2 = new Point(point.x+1,point.y-2);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x+1,point.y-2).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x+1,point.y-2).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x+1,point.y-2).setColor(Color.GREEN);
				}
			}
			else if(squarecontrol.getSquare(point.x+1,point.y-2).getPeca()==null){
				squarecontrol.getSquare(point.x+1,point.y-2).setColor(Color.GREEN);
			}
		}

		if(point.y<6 && point.x>=1){
			Point point2 = new Point(point.x-1,point.y+2);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x-1,point.y+2).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x-1,point.y+2).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x-1,point.y+2).setColor(Color.GREEN);
				}
			}
			else if(squarecontrol.getSquare(point.x-1,point.y+2).getPeca()==null){
				squarecontrol.getSquare(point.x-1,point.y+2).setColor(Color.GREEN);
			}
		}

		if(point.y>1 && point.x>=1){
			Point point2 = new Point(point.x-1,point.y-2);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x-1,point.y-2).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x-1,point.y-2).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x-1,point.y-2).setColor(Color.GREEN);
				}
			}
			else if(squarecontrol.getSquare(point.x-1,point.y-2).getPeca()==null){
				squarecontrol.getSquare(point.x-1,point.y-2).setColor(Color.GREEN);
			}
		}

		if(point.x>1 && point.y>0){
			Point point2 = new Point(point.x-2,point.y-1);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x-2,point.y-1).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x-2,point.y-1).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x-2,point.y-1).setColor(Color.GREEN);
				}
			}
			else if(squarecontrol.getSquare(point.x-2,point.y-1).getPeca()==null){
				squarecontrol.getSquare(point.x-2,point.y-1).setColor(Color.GREEN);
			}
		}

		if(point.x>1 && point.y<7){
			Point point2 = new Point(point.x-2,point.y+1);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x-2,point.y+1).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x-2,point.y+1).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x-2,point.y+1).setColor(Color.GREEN);
				}
			}
			else if(squarecontrol.getSquare(point.x-2,point.y+1).getPeca()==null){
				squarecontrol.getSquare(point.x-2,point.y+1).setColor(Color.GREEN);
			}
		}

		if(point.x<6 && point.y>0){
			Point point2 = new Point(point.x+2,point.y-1);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x+2,point.y-1).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x+2,point.y-1).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x+2,point.y-1).setColor(Color.GREEN);
				}
			}
			else if(squarecontrol.getSquare(point.x+2,point.y-1).getPeca()==null){
				squarecontrol.getSquare(point.x+2,point.y-1).setColor(Color.GREEN);
			}	
		}

		if(point.x<6 && point.y<7){
			Point point2 = new Point(point.x+2,point.y+1);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x+2,point.y+1).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x+2,point.y+1).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x+2,point.y+1).setColor(Color.GREEN);
				}
			}
			else if(squarecontrol.getSquare(point.x+2,point.y+1).getPeca()==null){
				squarecontrol.getSquare(point.x+2,point.y+1).setColor(Color.GREEN);
			}	
		}

		return moves;	
	}

	@Override
	public void tstXeque(SquareControl squarecontrol,Color cor,Square square){

		Point point = square.getPosition();


		if(point.y<6 && point.x<7){
			if(squarecontrol.getSquare(point.x+1,point.y+2).getPeca()!=null){
				if(squarecontrol.getSquare(point.x+1,point.y+2).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(point.x+1,point.y+2).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(point.x+1,point.y+2).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
						}
					}
				}
			}
		}
		if(point.y>1 && point.x<7){
			if(squarecontrol.getSquare(point.x+1,point.y-2).getPeca()!=null){
				if(squarecontrol.getSquare(point.x+1,point.y-2).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(point.x+1,point.y-2).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(point.x+1,point.y-2).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
						}
					}
				}
			}
		}
		if(point.y<6 && point.x>=1){
			if(squarecontrol.getSquare(point.x-1,point.y-2).getPeca()!=null){
				if(squarecontrol.getSquare(point.x-1,point.y-2).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(point.x-1,point.y+2).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(point.x-1,point.y+2).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
						}
					}
				}
			}
		}
		if(point.y>1 && point.x>=1){
			if(squarecontrol.getSquare(point.x-1,point.y-2).getPeca()!=null){
				if(squarecontrol.getSquare(point.x-1,point.y-2).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(point.x-1,point.y-2).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(point.x-1,point.y-2).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
						}
					}
				}
			}
		}

		if(point.x>1 && point.y>0){
			if(squarecontrol.getSquare(point.x-2,point.y-1).getPeca()!=null){
				if(squarecontrol.getSquare(point.x-2,point.y-1).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(point.x-2,point.y-1).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(point.x-2,point.y-1).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
						}
					}
				}
			}
		}
		if(point.x>1 && point.y<7){
			if(squarecontrol.getSquare(point.x-2,point.y+1).getPeca()!=null){
				if(squarecontrol.getSquare(point.x-2,point.y+1).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(point.x-2,point.y+1).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(point.x-2,point.y+1).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
						}
					}
				}
			}
		}
		if(point.x<6 && point.y>0){
			if(squarecontrol.getSquare(point.x+2,point.y-1).getPeca()!=null){
				if(squarecontrol.getSquare(point.x+2,point.y-1).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(point.x+2,point.y-1).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(point.x+2,point.y-1).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
						}
					}
				}
			}
		}
		if(point.x<6 && point.y<7){
			if(squarecontrol.getSquare(point.x+2,point.y+1).getPeca()!=null){
				if(squarecontrol.getSquare(point.x+2,point.y+1).getPeca().getCor()!=cor){
					if(squarecontrol.getSquare(point.x+2,point.y+1).getPeca().getNome()=="Rei"){
						if(squarecontrol.getSquare(point.x+2,point.y+1).getPeca().getCor()==Color.BLACK){
							squarecontrol.mensagem2("Rei marrom em xeque");
							squarecontrol.valida(1);
						}
						else{
							squarecontrol.mensagem2("Rei branco em xeque");
							squarecontrol.valida(1);
						}
					}
				}
			}
		}

	}
}








