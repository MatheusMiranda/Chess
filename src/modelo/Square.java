package modelo;

import java.awt.Color;
import java.awt.Point;

public class Square {

	public interface SquareEventListener {

		public void onOutEvent(Square square);

		public void onHoverEvent(Square square);

		public void onSelectEvent(Square square);

	}

	public interface SquareChangeListener {
		public void onColorChange(Square square);
		public void onChangePeca(Square square);
	}

	public static final int DEFAULT_SIZE = 64;
	public static final Color DEFAULT_COLOR = Color.WHITE;
	public static final SquareEventListener EMPTY_EVENT_LISTENER = null;
	public static final SquareChangeListener EMPTY_CHANGE_LISTENER = null;

	private Color color;
	private Integer size;
	private Point position;

	private SquareEventListener squareEventListener;
	private SquareChangeListener squareChangeListener;

	private Peca p;
	private static final Peca SEM_PECA = null;

	public Square() {
		this(0, 0);
	}

	public Square(int x, int y) {
		this(new Point(x, y));
	}

	public Square(Point position) {
		this(position, DEFAULT_COLOR);
	}

	public Square(int x, int y, Color color) {
		this(x, y, color, SEM_PECA);
	}

	public Square(Point position, Color color) {
		this(position, color, SEM_PECA);
	}

	public Square(int x, int y, Color color, int size) {
		this(x, y, color, SEM_PECA, size);
	}

	public Square(Point position, Color color, int size) {
		this(position, color, SEM_PECA, size);
	}

	public Square(int x, int y, Color color, Peca p) {
		this(new Point(x, y), color, p, DEFAULT_SIZE);
	}

	public Square(Point position, Color color, Peca p) {
		this(position, color, p, DEFAULT_SIZE);
	}

	public Square(int x, int y, Color color, Peca p, int size) {
		this(new Point(x, y), color, p, size);
	}

	public Square(Point position, Color color, Peca p, int size) {
		this.size = size;
		this.color = color;
		this.position = position;
		this.p = p;

		this.squareEventListener = EMPTY_EVENT_LISTENER;
	}


	public void removePeca() {
		this.p = SEM_PECA;
		notifyOnChangePeca();
	}

	public void notifyOnOutEvent() {
		if (haveSquereEventListener()) {
			this.squareEventListener.onOutEvent(this);
		}
	}

	public void notifyOnHoverEvent() {
		if (haveSquereEventListener()) {
			this.squareEventListener.onHoverEvent(this);
		}
	}

	public void notifyOnSelectEvent() {
		if (haveSquereEventListener()) {
			this.squareEventListener.onSelectEvent(this);
		}
	}

	public void notifyOnColorChange() {
		if (haveSquareChangeListener()) {
			this.squareChangeListener.onColorChange(this);
		}
	}

	public void notifyOnChangePeca() {
		if (haveSquareChangeListener()) {
			this.squareChangeListener.onChangePeca(this);
		}
	}

	public Point getPosition() {
		return this.position;
	}

	public Integer getSize() {
		return this.size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public boolean havePeca() {
		return this.p != SEM_PECA;
	}



	public Peca getPeca(){
		return this.p;
	}


	public void setPeca(Peca p){
		this.p = p;
		notifyOnChangePeca();
	}

	public void descobrirOTipo(){

	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
		notifyOnColorChange();
	}

	public void setSquareEventListener(SquareEventListener squareEventListener) {
		this.squareEventListener = squareEventListener;
	}

	public void setSquareChangeListener(SquareChangeListener squareChangeListener) {
		this.squareChangeListener = squareChangeListener;
	}

	private boolean haveSquereEventListener() {
		return this.squareEventListener != EMPTY_EVENT_LISTENER;
	}

	private boolean haveSquareChangeListener() {
		return this.squareChangeListener != EMPTY_CHANGE_LISTENER;
	}
}
