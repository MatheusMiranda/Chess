package modelo;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import control.SquareControl;

public class Rei extends Peca {
	public Rei(String nome,String piecePath,Color cor){
		super(nome,piecePath,cor);
	}

	@Override
	public ArrayList<Point> move(SquareControl squarecontrol,Square square){

		Point point = square.getPosition();
		square.setColor(Color.GREEN);	
		ArrayList<Point> moves = new ArrayList<Point>();			

		if(point.x<7 && point.y<7){
			Point point2 = new Point(point.x+1,point.y+1);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x+1,point.y+1).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x+1,point.y+1).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x+1,point.y+1).setColor(Color.GREEN);
				}
			}	
			else if(squarecontrol.getSquare(point.x+1,point.y+1).getPeca()==null){
				squarecontrol.getSquare(point.x+1,point.y+1).setColor(Color.GREEN);
			}
		}

		if(point.x<7){
			Point point2 = new Point(point.x+1,point.y);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x+1,point.y).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x+1,point.y).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x+1,point.y).setColor(Color.GREEN);
				}
			}	
			else if(squarecontrol.getSquare(point.x+1,point.y).getPeca()==null){
				squarecontrol.getSquare(point.x+1,point.y).setColor(Color.GREEN);
			}		
		}

		if(point.x<7 && point.y>0){
			Point point2 = new Point(point.x+1,point.y-1);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x+1,point.y-1).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x+1,point.y-1).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x+1,point.y-1).setColor(Color.GREEN);
				}
			}	
			else if(squarecontrol.getSquare(point.x+1,point.y-1).getPeca()==null){
				squarecontrol.getSquare(point.x+1,point.y-1).setColor(Color.GREEN);
			}
		}

		if(point.y<7){
			Point point2 = new Point(point.x,point.y+1);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x,point.y+1).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x,point.y+1).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x,point.y+1).setColor(Color.GREEN);
				}
			}	
			else if(squarecontrol.getSquare(point.x,point.y+1).getPeca()==null){
				squarecontrol.getSquare(point.x,point.y+1).setColor(Color.GREEN);
			}
		}

		if(point.x>0 && point.y>0){
			Point point2 = new Point(point.x-1,point.y-1);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x-1,point.y-1).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x-1,point.y-1).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x-1,point.y-1).setColor(Color.GREEN);
				}
			}	
			else if(squarecontrol.getSquare(point.x-1,point.y-1).getPeca()==null){
				squarecontrol.getSquare(point.x-1,point.y-1).setColor(Color.GREEN);
			}
		}

		if(point.x>0){
			Point point2 = new Point(point.x-1,point.y);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x-1,point.y).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x-1,point.y).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x-1,point.y).setColor(Color.GREEN);
				}
			}	
			else if(squarecontrol.getSquare(point.x-1,point.y).getPeca()==null){
				squarecontrol.getSquare(point.x-1,point.y).setColor(Color.GREEN);
			}
		}

		if(point.x>0 && point.y<7){
			Point point2 = new Point(point.x-1,point.y+1);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x-1,point.y+1).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x-1,point.y+1).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x-1,point.y+1).setColor(Color.GREEN);
				}
			}	
			else if(squarecontrol.getSquare(point.x-1,point.y+1).getPeca()==null){
				squarecontrol.getSquare(point.x-1,point.y+1).setColor(Color.GREEN);
			}
		}

		if(point.y>0){
			Point point2 = new Point(point.x,point.y-1);
			moves.add(point2);
			if(squarecontrol.getSquare(point.x,point.y-1).getPeca()!=null){
				Color cor = square.getPeca().getCor();
				if(squarecontrol.getSquare(point.x,point.y-1).getPeca().getCor()!=cor){
					squarecontrol.getSquare(point.x,point.y-1).setColor(Color.GREEN);
				}
			}	
			else if(squarecontrol.getSquare(point.x,point.y-1).getPeca()==null){
				squarecontrol.getSquare(point.x,point.y-1).setColor(Color.GREEN);
			}
		}

		return moves;
	}


	@Override
	public void tstXeque(SquareControl squarecontrol,Color cor,Square square){}	

}
