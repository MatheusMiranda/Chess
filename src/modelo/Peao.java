package modelo;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import control.SquareControl;

public class Peao extends Peca{

	public Peao(String nome,String piecePath,Color cor){
		super(nome,piecePath,cor);	
	}

	public ArrayList<Point> move(SquareControl squarecontrol,Square square){

		Point point = square.getPosition();
		square.setColor(Color.GREEN);	
		ArrayList<Point> moves = new ArrayList<Point>();

		Color cor2 = square.getPeca().getCor();

		if(cor2==Color.BLACK){

			if(point.x == 1){

				if(point.y>0 && point.x<=6){
					Point point2 = new Point(point.x+1,point.y-1);
					moves.add(point2);

					if(squarecontrol.getSquare(point.x+1,point.y-1).getPeca()!=null){
						Color cor = square.getPeca().getCor();
						if(squarecontrol.getSquare(point.x+1,point.y-1).getPeca().getCor()!=cor){
							squarecontrol.getSquare(point.x+1,point.y-1).setColor(Color.GREEN);
						}
					}

				}
				if(point.y<=6 && point.x<=6){
					Point point2 = new Point(point.x+1,point.y+1);
					moves.add(point2);

					if(squarecontrol.getSquare(point.x+1,point.y+1).getPeca()!=null){
						Color cor = square.getPeca().getCor();
						if(squarecontrol.getSquare(point.x+1,point.y+1).getPeca().getCor()!=cor){
							squarecontrol.getSquare(point.x+1,point.y+1).setColor(Color.GREEN);
						}
					}
				}

				for(int i = point.x+1; i<=3; i++){
					Point point2 = new Point(i,point.y);
					moves.add(point2);

					if(squarecontrol.getSquare(i,point.y).getPeca()==null){
						squarecontrol.getSquare(i,point.y).setColor(Color.GREEN);
					}
					else{
						break;
					}
				}		

			}
			else{
				if(point.y>0 && point.x<=6){
					Point point2 = new Point(point.x+1,point.y-1);
					moves.add(point2);

					if(squarecontrol.getSquare(point.x+1,point.y-1).getPeca()!=null){
						Color cor = square.getPeca().getCor();
						if(squarecontrol.getSquare(point.x+1,point.y-1).getPeca().getCor()!=cor){
							squarecontrol.getSquare(point.x+1,point.y-1).setColor(Color.GREEN);
						}
					}
				}
				if(point.y<=6 && point.x<=6){
					Point point2 = new Point(point.x+1,point.y+1);
					moves.add(point2);

					if(squarecontrol.getSquare(point.x+1,point.y+1).getPeca()!=null){
						Color cor = square.getPeca().getCor();
						if(squarecontrol.getSquare(point.x+1,point.y+1).getPeca().getCor()!=cor){
							squarecontrol.getSquare(point.x+1,point.y+1).setColor(Color.GREEN);
						}
					}
				}
				if(point.x<7){
					Point point2 = new Point(point.x+1,point.y);
					moves.add(point2);

					if(squarecontrol.getSquare(point.x+1,point.y).getPeca()==null){
						squarecontrol.getSquare(point.x+1,point.y).setColor(Color.GREEN);
					}

				}
			}

		}
		else{

			if(point.x == 6){
				if(point.x>0 && point.y>0){
					Point point2 = new Point(point.x-1,point.y-1);
					moves.add(point2);

					if(squarecontrol.getSquare(point.x-1,point.y-1).getPeca()!=null){
						Color cor = square.getPeca().getCor();
						if(squarecontrol.getSquare(point.x-1,point.y-1).getPeca().getCor()!=cor){
							squarecontrol.getSquare(point.x-1,point.y-1).setColor(Color.GREEN);
						}
					}
				}
				if(point.x>0 && point.y<7){
					Point point2 = new Point(point.x-1,point.y+1);
					moves.add(point2);

					if(squarecontrol.getSquare(point.x-1,point.y+1).getPeca()!=null){
						Color cor = square.getPeca().getCor();
						if(squarecontrol.getSquare(point.x-1,point.y+1).getPeca().getCor()!=cor){
							squarecontrol.getSquare(point.x-1,point.y+1).setColor(Color.GREEN);
						}
					}
				}	

				for(int i = point.x-1; i>=4; i--){
					Point point2 = new Point(i,point.y);
					moves.add(point2);

					if(squarecontrol.getSquare(i,point.y).getPeca()==null){
						squarecontrol.getSquare(i,point.y).setColor(Color.GREEN);
					}
					else{
						break;
					}
				}

			}
			else{
				if(point.x>0 && point.y>0){
					Point point2 = new Point(point.x-1,point.y-1);
					moves.add(point2);

					if(squarecontrol.getSquare(point.x-1,point.y-1).getPeca()!=null){
						Color cor = square.getPeca().getCor();
						if(squarecontrol.getSquare(point.x-1,point.y-1).getPeca().getCor()!=cor){
							squarecontrol.getSquare(point.x-1,point.y-1).setColor(Color.GREEN);
						}
					}
				}
				if(point.x>0 && point.y<7){
					Point point2 = new Point(point.x-1,point.y+1);
					moves.add(point2);

					if(squarecontrol.getSquare(point.x-1,point.y+1).getPeca()!=null){
						Color cor = square.getPeca().getCor();
						if(squarecontrol.getSquare(point.x-1,point.y+1).getPeca().getCor()!=cor){
							squarecontrol.getSquare(point.x-1,point.y+1).setColor(Color.GREEN);
						}
					}
				}
				if(point.x>0){
					Point point2 = new Point(point.x-1,point.y);
					moves.add(point2);

					if(squarecontrol.getSquare(point.x-1,point.y).getPeca()==null){
						squarecontrol.getSquare(point.x-1,point.y).setColor(Color.GREEN);
					}
				}


			}


		}
		return moves;
	}

	@Override
	public void tstXeque(SquareControl squarecontrol,Color cor,Square square){
		Point point = square.getPosition();

		if(cor==Color.BLACK){

			if(point.y>0 && point.x<=6){
				if(squarecontrol.getSquare(point.x+1,point.y-1).getPeca()!=null){
					if(squarecontrol.getSquare(point.x+1,point.y-1).getPeca().getCor()!=cor){
						if(squarecontrol.getSquare(point.x+1,point.y-1).getPeca().getNome()=="Rei"){
							if(squarecontrol.getSquare(point.x+1,point.y-1).getPeca().getCor()==Color.BLACK){
								squarecontrol.mensagem2("Rei marrom em xeque");
								squarecontrol.valida(1);
							}
							else{
								squarecontrol.mensagem2("Rei branco em xeque");
								squarecontrol.valida(1);
							}
						}
					}
				}
			}
			if(point.y<=6 && point.x<=6){
				if(squarecontrol.getSquare(point.x+1,point.y+1).getPeca()!=null){
					if(squarecontrol.getSquare(point.x+1,point.y+1).getPeca().getCor()!=cor){
						if(squarecontrol.getSquare(point.x+1,point.y+1).getPeca().getNome()=="Rei"){
							if(squarecontrol.getSquare(point.x+1,point.y+1).getPeca().getCor()==Color.BLACK){
								squarecontrol.mensagem2("Rei marrom em xeque");
								squarecontrol.valida(1);
							}
							else{
								squarecontrol.mensagem2("Rei branco em xeque");
								squarecontrol.valida(1);
							}
						}
					}
				}
			}

		}	

		else{

			if(point.x>0 && point.y>0){
				if(squarecontrol.getSquare(point.x-1,point.y-1).getPeca()!=null){
					if(squarecontrol.getSquare(point.x-1,point.y-1).getPeca().getCor()!=cor){
						if(squarecontrol.getSquare(point.x-1,point.y-1).getPeca().getNome()=="Rei"){
							if(squarecontrol.getSquare(point.x+1,point.y-1).getPeca().getCor()==Color.BLACK){
								squarecontrol.mensagem2("Rei marrom em xeque");
								squarecontrol.valida(1);
							}
							else{
								squarecontrol.mensagem2("Rei branco em xeque");
								squarecontrol.valida(1);
							}
						}
					}
				}
			}
			if(point.x>0 && point.y<=6){
				if(squarecontrol.getSquare(point.x-1,point.y+1).getPeca()!=null){
					if(squarecontrol.getSquare(point.x-1,point.y+1).getPeca().getCor()!=cor){
						if(squarecontrol.getSquare(point.x-1,point.y+1).getPeca().getNome()=="Rei"){
							if(squarecontrol.getSquare(point.x-1,point.y+1).getPeca().getCor()==Color.BLACK){
								squarecontrol.mensagem2("Rei marrom em xeque");
								squarecontrol.valida(1);
							}
							else{
								squarecontrol.mensagem2("Rei branco em xeque");
								squarecontrol.valida(1);
							}
						}
					}
				}
			}

		}

	}


}

